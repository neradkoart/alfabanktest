package com.test.utils;

public class Sorter {

    private static int array[];
    private static int length;

    public static int[] sortAsc(int[] inputArr) {

        if (inputArr == null || inputArr.length == 0) {
            return inputArr;
        }
        array = inputArr;
        length = inputArr.length;
        quickSortAsc(0, length - 1);
        return array;
    }

    private static void quickSortAsc(int lowerIndex, int higherIndex) {
        int i = lowerIndex;
        int j = higherIndex;
        int pivot = array[lowerIndex+(higherIndex-lowerIndex)/2];
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                exchangeNumbers(i, j);
                i++;
                j--;
            }
        }

        if (lowerIndex < j)
            quickSortAsc(lowerIndex, j);
        if (i < higherIndex)
            quickSortAsc(i, higherIndex);
    }

    private static void exchangeNumbers(int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static int[] sortDesc(int[] inputArr) {

        if (inputArr == null || inputArr.length == 0) {
            return inputArr;
        }
        array = inputArr;
        length = inputArr.length;
        quickSortDesc(0, length - 1);
        return array;
    }

    private static void quickSortDesc(int lowerIndex, int higherIndex) {
        int i = lowerIndex;
        int j = higherIndex;
        int pivot = array[lowerIndex+(higherIndex-lowerIndex)/2];
        while (i <= j) {
            while (array[i] > pivot) {
                i++;
            }
            while (array[j] < pivot) {
                j--;
            }
            if (i <= j) {
                exchangeNumbers(i, j);
                i++;
                j--;
            }
        }

        if (lowerIndex < j)
            quickSortDesc(lowerIndex, j);
        if (i < higherIndex)
            quickSortDesc(i, higherIndex);
    }

}
