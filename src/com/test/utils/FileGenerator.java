package com.test.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class FileGenerator {

    ArrayList<Integer> numbers = new ArrayList<>();
    int count;

    public FileGenerator(int count) {
        this.count = count;
        for (int i = 0; i <= count; i++) {
            numbers.add(i);
        }
    }

    public void generateFile() {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("example.txt"), "utf-8"))) {
            StringBuilder string = new StringBuilder();

            do {
                int a = new Random().nextInt(count);
                string.append(numbers.get(a)).append(",");
                count--;
                numbers.remove(a);
            } while (count > 0);

            writer.write(string.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
