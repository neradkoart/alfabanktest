package com.test;

import com.test.utils.FileGenerator;
import com.test.utils.Sorter;

import java.io.*;

public class Main {

    private static final int COUNT = 20;

    public static void main(String[] args) {
        new FileGenerator(COUNT).generateFile();
        int[] array = readFile("example.txt");
        System.out.println("Unsorted array: ");
        printArray(array);

        int[] sortedAscArray = Sorter.sortAsc(array);
        System.out.print("Sorted asc array: ");
        printArray(sortedAscArray);

        int[] sortedDescArray = Sorter.sortDesc(array);
        System.out.print("Sorted desc array: ");
        printArray(sortedDescArray);

        new File("example.txt").delete();

        writeFactorial(20);
    }

    private static int[] readFile(String filename) {
        int[] array = new int[COUNT];
        try {
            FileReader reader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();

            String[] nums = line.split(",");

            for (int i = 0; i < nums.length; i++) {
                array[i] = Integer.parseInt(nums[i]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    private static void writeFactorial(int number){
        long factorial = 1;
        for (int i = 1; i < number; i++, factorial *= i);
        System.out.println("Factorial of " + number + " = " + factorial);
    }

    private static void printArray(int[] array){
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("\n");
    }
